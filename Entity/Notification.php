<?php

namespace Lexik\Bundle\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
abstract class Notification
{
    /**
     * Id
     */
    protected $id;

    /**
     * Relation to the user.
     */
    protected $user;

    /**
     * @ORM\Column(type="text", name="content")
     */
    protected $content;

    /**
     * @ORM\Column(type="boolean", name="is_new")
     */
    protected $isNew;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * Construct.
     */
    public function __construct()
    {
        $this->isNew = true;
        $this->createdAt = new \DateTime();
    }

    /**
     * toString
     *
     * @return string
     */
    public function __toString()
    {
        return $this->content;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     */
    public function setIsNew($isNew)
    {
        $this->isNew = (boolean) $isNew;
    }

    /**
     * Get isNew
     *
     * @return boolean
     */
    public function getIsNew()
    {
        return (boolean) $this->isNew;
    }

    /**
     * IsNew
     *
     * @return boolean
     */
    public function isNew()
    {
        return (boolean) $this->isNew;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
