<?php

namespace Lexik\Bundle\NotificationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Collect all rederers.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class RendererPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('lexik_notification.renderer_collector')) {
            $definition = $container->findDefinition('lexik_notification.renderer_collector');

            foreach ($container->findTaggedServiceIds('lexik_notification.renderer') as $id => $attributes) {
                $definition->addMethodCall('add', array($id, new Reference($id)));
            }
        }
    }
}
