<?php

namespace Lexik\Bundle\NotificationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Collect all notifier.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class NotifierPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('lexik_notification.notifier_collector')) {
            $definition = $container->findDefinition('lexik_notification.notifier_collector');

            foreach ($container->findTaggedServiceIds('lexik_notification.notifier') as $id => $attributes) {
                $definition->addMethodCall('add', array($attributes[0]['channel'], new Reference($id)));
            }
        }
    }
}
