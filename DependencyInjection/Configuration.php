<?php

namespace Lexik\Bundle\NotificationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lexik_notification');

        $rootNode
            ->children()

                ->scalarNode('notification_entity')
                    ->isRequired()
                ->end()

                ->arrayNode('default')
                    ->children()
                        ->arrayNode('notify_through')
                            ->treatNullLike(array())
                            ->prototype('scalar')->end()
                        ->end()
                        ->arrayNode('channels')
                            ->treatNullLike(array())
                            ->useAttributeAsKey('name')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('renderer')->isRequired()->end()
                                    ->scalarNode('template')->isRequired()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('events')
                    ->useAttributeAsKey('key')
                    ->prototype('array')
                        ->treatNullLike(array())
                        ->children()
                            ->arrayNode('notify_through')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('channels')
                                ->treatNullLike(array())
                                ->useAttributeAsKey('name')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('renderer')->end()
                                        ->scalarNode('template')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

            ->end()
        ;

        return $treeBuilder;
    }
}
