<?php

namespace Lexik\Bundle\NotificationBundle\Recipient;

/**
 * Notification's recipient interface.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
interface RecipientInterface
{
    /**
     * Returns true if the target is a single recipient, or false in case of multiple recipients.
     *
     * @return boolean
     */
    public function isSingleRecipient();

    /**
     * Returns the recipient or a collection of recipient.
     *
     * @return mixed
     */
    public function getRecipientData();
}
