<?php

namespace Lexik\Bundle\NotificationBundle\Recipient;

use Doctrine\ORM\Query;

/**
 * Send notifications to multiple recipient by using the given query.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class DoctrineRecipients implements RecipientInterface
{
    /**
     * @var Query
     */
    private $query;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var int
     */
    private $hydrationMode;

    /**
     * Construct.
     *
     * @param Query $query
     */
    public function __construct(Query $query)
    {
        $this->query = $query;
        $this->hydrationMode = Query::HYDRATE_OBJECT;
        $this->parameters = null;
    }

    /**
     * {@inheritdoc}
     */
    public function isSingleRecipient()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getRecipientData()
    {
        return $this->query->iterate($this->parameters, $this->hydrationMode);
    }
}
