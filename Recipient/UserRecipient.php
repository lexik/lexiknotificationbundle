<?php

namespace Lexik\Bundle\NotificationBundle\Recipient;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Send a notification to the given user.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class UserRecipient implements RecipientInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * Construct.
     *
     * @param User $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function isSingleRecipient()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getRecipientData()
    {
        return $this->user;
    }
}
