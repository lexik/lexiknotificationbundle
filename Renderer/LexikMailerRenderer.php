<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

use Lexik\Bundle\MailerBundle\Message\MessageFactory;
use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;
use Lexik\Bundle\NotificationBundle\Renderer\RendererInterface;

/**
 * Generate the content of a notification to send
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class LexikMailerRenderer implements RendererInterface
{
    /**
     * @var MessageFactory
     */
    private $messenger;

    /**
     * Constructor
     *
     * @param MessageFactory $messenger
     */
    public function __construct(MessageFactory $messenger)
    {
        $this->messenger = $messenger;
    }

    /**
     * {@inheritdoc}
     */
    public function render($eventKey, $payload, $template, RecipientInterface $user)
    {
        return $this->messenger->get($template, $user, $payload);
    }
}
