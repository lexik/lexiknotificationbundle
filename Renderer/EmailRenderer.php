<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;

/**
 * Generate a Swift_Message.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class EmailRenderer extends TwigTemplateRenderer
{
    /**
     * {@inheritdoc}
     */
    public function render($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        $body = parent::render($eventKey, $payload, $template, $recipient);

        $subject = isset($payload['subject']) ? $payload['subject'] : 'subject';
        $contentType = isset($payload['content_type']) ? $payload['content_type'] : 'text/html';

        $message = \Swift_Message::newInstance($subject, $body, $contentType);
        $message->setTo($payload['to']);
        $message->setFrom($payload['from']);

        return $message;
    }
}
