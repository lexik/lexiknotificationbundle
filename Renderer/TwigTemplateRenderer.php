<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;
use Lexik\Bundle\NotificationBundle\Renderer\RendererInterface;

use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Generate the content of a notification from a twig template.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class TwigTemplateRenderer implements RendererInterface
{
    /**
     * @var TwigEngine
     */
    protected $templating;

    /**
     * Constructor
     *
     * @param TwigEngine $templating
     */
    public function __construct(TwigEngine $templating)
    {
        $this->templating = $templating;
    }

    /**
     * {@inheritdoc}
     */
    public function render($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        return $this->templating->render($template, array(
            'payload'  => $payload,
            'eventKey' => $eventKey,
        ));
    }
}
