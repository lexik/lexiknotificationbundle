<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;

/**
 * Interface for rederers.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
interface RendererInterface
{
    /**
     * Render the notification content.
     *
     * @param  string             $eventKey
     * @param  mixed              $payload
     * @param  string             $template
     * @param  RecipientInterface $recipient
     * @return mixed
     */
    public function render($eventKey, $payload, $template, RecipientInterface $recipient);
}
