<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

/**
 * Collect all renderers.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class RendererCollector
{
    /**
     * @var array
     */
    private $renderers;

    /**
     * Construct.
     */
    public function __construct()
    {
        $this->renderers = array();
    }

    /**
     * Add a notifier
     *
     * @param int               $id
     * @param RendererInterface $notifier
     */
    public function add($id, RendererInterface $renderer)
    {
        $this->renderers[$id] = $renderer;
    }

    /**
     * Returns a renderer by its id.
     *
     * @param  int               $id
     * @throws \RuntimeException
     * @return RendererInterface
     */
    public function get($id)
    {
        if ( ! $this->has($id) ) {
            throw new \RuntimeException(sprintf('Unknown notification renderer "%s"', $id));
        }

        return $this->renderers[$id];
    }

    /**
     * Returns true if the renderer exist.
     *
     * @param  int     $id
     * @return boolean
     */
    public function has($id)
    {
        return isset($this->renderers[$id]);
    }
}
