<?php

namespace Lexik\Bundle\NotificationBundle\Renderer;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;

use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Generate a new Notification entity.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class EntityRenderer extends TwigTemplateRenderer
{
    /**
     * @var string
     */
    protected $class;

    /**
     *
     * @param TwigEngine $templating
     * @param string     $class
     */
    public function __construct(TwigEngine $templating, $class)
    {
        parent::__construct($templating);

        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function render($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        return $this->createNotification($eventKey, $payload, $template, $recipient);
    }

    /**
     * Create a new notification instance.
     *
     * @param  string             $eventKey
     * @param  mixed              $payload
     * @param  string             $template
     * @param  RecipientInterface $user
     * @return Notification
     */
    protected function createNotification($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        $content = parent::render($eventKey, $payload, $template, $recipient);

        $notificationClass = $this->class;

        $notification = new $notificationClass();
        $notification->setUser($recipient->getRecipientData());
        $notification->setContent($content);

        return $notification;
    }
}
