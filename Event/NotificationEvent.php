<?php
namespace Lexik\Bundle\NotificationBundle\Event;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;

use Symfony\Component\EventDispatcher\Event;

/**
 * Notification Event
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class NotificationEvent extends Event
{
    /**
     * @var string
     */
    private $eventKey;

    /**
     * @var mixed
     */
    private $payload;

    /**
     * @var RecipientInterface
     */
    private $recipient;

    /**
     * Constructor
     *
     * @param string             $eventKey
     * @param type               $payload
     * @param RecipientInterface $recipient
     */
    public function __construct($eventKey, $payload, RecipientInterface $recipient)
    {
        $this->eventKey  = $eventKey;
        $this->payload   = $payload;
        $this->recipient = $recipient;
    }

    /**
     * Get the event key
     *
     * @return string
     */
    public function getEventKey()
    {
        return $this->eventKey;
    }

    /**
     * Get payload
     *
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Get the user
     *
     * @return RecipientInterface
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
}
