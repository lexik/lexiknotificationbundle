<?php
namespace Lexik\Bundle\NotificationBundle\Notifier;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;
use Lexik\Bundle\NotificationBundle\Renderer\RendererInterface;

/**
 * Interface for channel notifiers.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
Interface NotifierInterface
{
    /**
     * Set the renderer instance.
     *
     * @param RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer);

    /**
     * Handle the notification for given parameters.
     *
     * @param string             $eventKey
     * @param mixed              $payload
     * @param string             $template
     * @param RecipientInterface $recipient
     */
    public function notify($eventKey, $payload, $template, RecipientInterface $recipient);
}
