<?php

namespace Lexik\Bundle\NotificationBundle\Notifier;

use Doctrine\Common\Persistence\ObjectManager;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;
use Lexik\Bundle\NotificationBundle\Notifier\NotifierInterface;
use Lexik\Bundle\NotificationBundle\Renderer\RendererInterface;
use Lexik\Bundle\NotificationBundle\Entity\Notification;

/**
 * Insert a new notification in the database.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class DatabaseNotifier implements NotifierInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * Constructor
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * {@inheritdoc}
     */
    public function notify($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        if ($recipient->isSingleRecipient()) {
            $notification = $this->renderer->render($eventKey, $payload, $template, $recipient);
            $this->om->persist($notification);
            $this->om->flush($notification);

        } else {
            $i = 0;
            $iterableResult = $recipient->getRecipientData();

            foreach ($iterableResult as $user) {
                $notification = $this->renderer->render($eventKey, $payload, $template, new UserRecipient($user[0]));
                $this->om->persist($notification);

                if ( ($i % 30) == 0 ) {
                    $this->om->flush();
                    $this->om->clear(get_class($notification));
                }

                $i++;
            }

            $this->om->flush();
        }
    }
}
