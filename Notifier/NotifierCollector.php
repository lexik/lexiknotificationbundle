<?php

namespace Lexik\Bundle\NotificationBundle\Notifier;

/**
 * Collect all channel notifiers.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class NotifierCollector
{
    /**
     * @var array
     */
    private $notifiers;

    /**
     * Construct.
     */
    public function __construct()
    {
        $this->notifiers = array();
    }

    /**
     * Add a channel notifier
     *
     * @param string            $channel
     * @param NotifierInterface $notifier
     */
    public function add($channel, NotifierInterface $notifier)
    {
        $this->notifiers[$channel] = $notifier;
    }

    /**
     * Returns the notifier for the given channel.
     *
     * @param  string            $channel
     * @return NotifierInterface
     */
    public function get($channel)
    {
        if ( ! $this->has($channel) ) {
            throw new \RuntimeException(sprintf('No notifier found for channel "%s"', $channel));
        }

        return $this->notifiers[$channel];
    }

    /**
     * Returns true if the channel has a notifier.
     *
     * @param  string  $channel
     * @return boolean
     */
    public function has($channel)
    {
        return isset($this->notifiers[$channel]);
    }
}
