<?php

namespace Lexik\Bundle\NotificationBundle\Notifier;

use Lexik\Bundle\NotificationBundle\Recipient\RecipientInterface;
use Lexik\Bundle\NotificationBundle\Notifier\NotifierInterface;
use Lexik\Bundle\NotificationBundle\Renderer\RendererInterface;

/**
 * Notify via Email
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class EmailNotifier implements NotifierInterface
{
    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * Constructor
     *
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * {@inheritdoc}
     */
    public function notify($eventKey, $payload, $template, RecipientInterface $recipient)
    {
        $message = $this->renderer->render($eventKey, $payload, $template, $recipient);

        return $this->mailer->send($message);
    }
}
