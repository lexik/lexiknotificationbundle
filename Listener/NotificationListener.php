<?php
namespace Lexik\Bundle\NotificationBundle\Listener;

use Symfony\Bridge\Monolog\Logger;

use Lexik\Bundle\NotificationBundle\Renderer\RendererCollector;
use Lexik\Bundle\NotificationBundle\Notifier\NotifierCollector;
use Lexik\Bundle\NotificationBundle\Entity\Notification;
use Lexik\Bundle\NotificationBundle\Event\NotificationEvent;

/**
 * Listen notification events.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class NotificationListener
{
    /**
     * @var NotifierCollector
     */
    private $notifierCollector;

    /**
     * @var RendererCollector
     */
    private $rendererCollector;

    /**
     * @var array
     */
    private $default;

    /**
     * @var array
     */
    private $events;

    /**
     * @var Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $env;

    /**
     * Constructor
     *
     * @param NotifierCollector $notifierCollector
     * @param RendererCollector $rendererCollector
     * @param array             $default
     * @param array             $events
     * @param Logger            $logger
     * @param string            $env
     */
    public function __construct(NotifierCollector $notifierCollector, RendererCollector $rendererCollector, array $default, array $events, Logger $logger, $env)
    {
        $this->notifierCollector = $notifierCollector;
        $this->rendererCollector = $rendererCollector;

        $this->default = $default;
        $this->events  = $events;
        $this->logger  = $logger;
        $this->env     = $env;
    }

    /**
     * Process and dispatch the notification to the right channels
     *
     * @param string $eventKey
     * @param mixed  $payload
     * @param User   $user
     *
     * @throws \InvalidArgumentException
     */
    public function processNotification(NotificationEvent $event)
    {
        $eventKey  = $event->getEventKey();
        $payload   = $event->getPayload();
        $recipient = $event->getRecipient();

        $config = $this->parseConfig($event);

        // generate and process content for each channel
        foreach ($config['notify_through'] as $channel) {
            list($rendererId, $template) = $this->getRenderingData($config, $channel);

            $notifier = $this->notifierCollector->get($channel);
            $renderer = $this->rendererCollector->get($rendererId);

            try {
                $notifier->setRenderer($renderer);
                $notifier->notify($eventKey, $payload, $template, $recipient);
            } catch (\Exception $exp) {
                // re throw the exception if current environment is 'dev' else we log the error
                if ($this->env === 'dev') {
                    throw $exp;
                } else {
                    $this->logger->error($exp->getMessage(), $exp->getTrace());
                }
            }
        }
    }

    /**
     * Generate the configuration data to use for given Event
     *
     * @param  NotificationEvent $event
     * @return array
     */
    private function parseConfig(NotificationEvent $event)
    {
        $eventKey = $event->getEventKey();
        $mergedConfig = $this->default;

        // update/replace default values if specified
        if (isset($this->events[$eventKey])) {
            $eventConfig = $this->events[$eventKey];

            if (!empty($eventConfig['notify_through'])) {
                $mergedConfig['notify_through'] = $eventConfig['notify_through'];
            }

            foreach ($mergedConfig['channels'] as $channel => $channelConfig) {
                if (isset($eventConfig['channels'][$channel])) {
                    $mergedConfig['channels'][$channel] = array_merge($channelConfig, $eventConfig['channels'][$channel]);
                }
            }
        }

        return $mergedConfig;
    }

    /**
     * Returns an array with rendering data (renderer id, template name, ...) for a given channel.
     *
     * @param  array                     $config
     * @throws \InvalidArgumentException
     * @return array
     */
    private function getRenderingData(array $config, $channel)
    {
        if (!isset($config['channels'][$channel], $config['channels'][$channel]['template'])) {
            throw new \InvalidArgumentException(sprintf('Undefined template for channel "%s"', $channel));
        }

        if (!isset($config['channels'][$channel], $config['channels'][$channel]['renderer'])) {
            throw new \InvalidArgumentException(sprintf('Undefined renderer for channel "%s"', $channel));
        }

        return array(
            $config['channels'][$channel]['renderer'],
            $config['channels'][$channel]['template'],
        );
    }
}
