<?php

namespace Lexik\Bundle\NotificationBundle;

use Lexik\Bundle\NotificationBundle\DependencyInjection\Compiler\NotifierPass;
use Lexik\Bundle\NotificationBundle\DependencyInjection\Compiler\RendererPass;
use Lexik\Bundle\NotificationBundle\DependencyInjection\Compiler\LoggerPass;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class LexikNotificationBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new LoggerPass());
        $container->addCompilerPass(new RendererPass());
        $container->addCompilerPass(new NotifierPass());
    }
}
